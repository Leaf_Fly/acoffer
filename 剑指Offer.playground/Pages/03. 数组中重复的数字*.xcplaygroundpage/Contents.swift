import Foundation

/**
 找出数组中重复的数字。
 在一个长度为 n 的数组 nums 里的所有数字都在 0～n-1 的范围内。数组中某些数字是重复的，但不知道有几个数字重复了，也不知道每个数字重复了几次。请找出数组中任意一个重复的数字。

 链接：https://leetcode-cn.com/problems/shu-zu-zhong-zhong-fu-de-shu-zi-lcof

 输入：
 [2, 3, 1, 0, 2, 5, 3]
 输出：2 或 3
 2 <= n <= 100000
 */


//func findRepeatNumber(_ nums: [Int]) -> Int {
//    var set: Set = Set<Int>()
//
//    for index in nums {
//        if set.contains(index) {
//            return index
//        } else {
//            set.insert(index)
//        }
//    }
//    return 0
//}

//【优化点位】 因为数组都在n-1区间，所以可以创建一个n大小的数组，value为新数组的下标，++
func findRepeatNumber(_ nums: [Int]) -> Int {
    var list: Array = Array(repeating: 0, count: nums.count)

    for index in nums {
        if list[index] == 0 {
            list[index] += 1
        } else {
            return index
        }
    }
    return 0
}
findRepeatNumber([2, 3, 1, 0, 2, 5, 3])
//findRepeatNumber([3, 1, 2, 3])
