import Foundation

public class ListNode {
    public var val: Int
    public var next: ListNode?
    public init(_ val: Int) {
        self.val = val
        self.next = nil
    }
}

// 耗时 36ms
func reversePrint0(_ head: ListNode?) -> [Int] {
    var list = [Int]()
    var head = head
    
    while head != nil {
        list.insert(head!.val, at: 0)
        head = head!.next
    }
    return list
}


func reversePrint(_ head: ListNode?) -> [Int] {
    var nodes = [ListNode]()
    var list = [Int]()
    var head = head
    
    while head != nil {
        nodes.append(head!)
        head = head!.next
    }
    
    var count = nodes.count - 1
    while count >= 0 {
        list.append(nodes[count].val)
        count -= 1
    }
    return list
}
