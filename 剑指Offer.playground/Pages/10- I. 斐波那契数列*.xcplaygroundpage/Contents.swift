import Foundation

func fib(_ n: Int) -> Int {
    if n == 0 {
        return 0
    } else if n == 1 {
        return 1
    }
    var pre = 0
    var current = 1
    var temp = 0
    
    for _ in 2...n {
        temp = current + pre
        pre = current
        current = temp%1000000007
    }
    return current
    
}

fib(10)
